# Connect to Terraform AWS modules
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-2"
  access_key = "AKIAUBTK2C4SQXRDJ3P6"
  secret_key = "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"
}

# Create a VPC for production
resource "aws_vpc" "vpc-dev" {
 cidr_block = "10.10.0.0/16"
 tags = {
    Name = "odoo-dev"
  }
}

# Create Internet Gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc-dev.id
}

# Create Custom Route Table
resource "aws_route_table" "dev-route-table" {
  vpc_id = aws_vpc.vpc-dev.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  route {
    ipv6_cidr_block        = "::/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "routetable-dev"
  }
}

# Create a subnet
resource "aws_subnet" "subnet-dev" {
  vpc_id = aws_vpc.vpc-dev.id
  cidr_block = "10.10.0.0/24"
  availability_zone = "us-east-2a"
  map_public_ip_on_launch = "true"
  tags = {
    "Name" = "subnet-dev"
  }
}

# Associate subnet with Route Table
resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.subnet-dev.id
  route_table_id = aws_route_table.dev-route-table.id
}

# Create Security Group to allow port 22, 80, 443
resource "aws_security_group" "allow_web" {
  name        = "allow_web_traffic"
  description = "Allow Web inbound traffic"
  vpc_id      = aws_vpc.vpc-dev.id

  ingress {
      description      = "HTTPS"
      from_port        = 443
      to_port          = 443
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["0.0.0.0/0"]
  }
  
  ingress {
      description      = "HTTP"
      from_port        = 80
      to_port          = 80
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["0.0.0.0/0"]
  }
  
  ingress {
      description      = "SSH"
      from_port        = 22
      to_port          = 22
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      description      = "odoo port"
      from_port        = 10014
      to_port          = 10014
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      description      = "live chat port"
      from_port        = 20014
      to_port          = 20014
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["0.0.0.0/0"]
  }
  
  egress {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
  }
  
  tags = {
    Name = "allow_web"
  }
}

# Create a network interface with an IP in the subnet that was created in step 4
resource "aws_network_interface" "nic-odoo001" {
  subnet_id       = aws_subnet.subnet-dev.id
  private_ips     = ["10.10.0.100"]
  security_groups = [aws_security_group.allow_web.id]

}

# Create instance EC2 with type t2.micro
resource "aws_instance" "odooERP001" {
  ami           = "ami-00399ec92321828f5"
  instance_type = "t2.micro"
  availability_zone = "us-east-2a"
  key_name = "learnAWS01"
  tags = {
    Name = "odooERP001"
  }
  network_interface {
    device_index = 0
    network_interface_id = aws_network_interface.nic-odoo001.id
  }
  user_data = <<-EOF
              #!/bin/bash
              sudo apt update -y
              curl -fsSL https://get.docker.com -o get-docker.sh
              sudo sh get-docker.sh
              sudo systemctl start docker
              sudo systemctl enable docker
              sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
              sudo chmod +x /usr/local/bin/docker-compose
              sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
              docker-compose --version
              EOF

}